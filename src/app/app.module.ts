import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NotesService } from './services/notes.service';
import { AuthService } from './services/auth.service';
import { MessagingService } from './services/messaging.service';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatListModule, MatButtonModule, MatSnackBarModule, MatIconModule } from '@angular/material'
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { FormsModule } from '@angular/forms';


const firebaseConfig: any =  {
  apiKey: "AIzaSyByEsK-VzPvNSAhXDwZIGnTraiQ2sUp_g8",
  authDomain: "platzinotas-ec2b4.firebaseapp.com",
  databaseURL: "https://platzinotas-ec2b4.firebaseio.com",
  projectId: "platzinotas-ec2b4",
  storageBucket: "platzinotas-ec2b4.appspot.com",
  messagingSenderId: "388985662919"
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    MatOptionModule,
    MatSelectModule,
    MatListModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    MatIconModule
    // imports firebase/storage only needed for storage features
  ],
  providers: [
    NotesService,
    AuthService,
    MessagingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
