import { Component,OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { NotesService } from './services/notes.service';
import { AuthService } from './services/auth.service';
import { MessagingService } from './services/messaging.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  panelOpenState: boolean = false;

  categorias: any = [
    "trabajo",
    "personal"
  ]

  note:any = {}
  notes: any []

  message: any = {}

  constructor(private swUpdate: SwUpdate, 
              private notesService: NotesService,
              private snackbar :MatSnackBar,
              private AuthService: AuthService,
              private MessagingService:MessagingService
             ) {

    this.notesService.getNotes().valueChanges().subscribe((fbNotes) => {
      this.notes = fbNotes;
      console.log('​AppComponent -> fbNotes', fbNotes);
      
    })


    this.MessagingService.getPermission();
    this.MessagingService.receiveMessage();
    this.message = this.MessagingService.currentMessage;
  }

  ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe((v) => {
          window.location.reload();
      });
    }
  }

  saveNote() {
    console.log(this.note);
    if(!this.note.id){
      this.note.id = Date.now();
    }
    
    this.notesService.createNote(this.note)
    .then((data)=> {
      console.log('​AppComponent -> saveNote -> data', data);
      this.note = {};
      this.snackbar.open('Message archived', 'Undo', {
        duration: 3000
      });
    })
  }

  selectNote(note){
    this.note = note;
  }

  deleteNote(note){
    this.notesService.deleteNote(note)
    .then((data)=> {
      console.log('​AppComponent -> deleteNote -> data', data);
      this.note = {};
      this.snackbar.open('Message archived delete', 'Undo', {
        duration: 3000
      });
    })
  }

  login() {
    this.AuthService.loginWithFacebook();
  }
}




